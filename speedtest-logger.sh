#!/bin/bash

# setting constants
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
executable=$SCRIPT_DIR/speedtest
now=`date +%s`
nowFormatted=`date`
SET_LOG=""

# declare functions
function usage() {
        echo "USAGE:"
        echo "  ./speedtest [OPTIONS]"
        echo "OPTIONS:"
        echo "  -h ... show this help page (alt: --help)"
        echo "  -l ... log into file (alt: --log)"
        exit
}

function parse_args() {
#        echo "parsing args"
        for var in "$@"
        do
#               echo "arg[i]: $var"
                if [ \( "$var" == "-log" \) -o \( "$var" == "-l" \) ]; then
                        SET_LOG=true
			logfile=$HOME/speedtest.log
                elif [ \( "$var" == "--help" \) -o \( "$var" == "-h" \) ]; then
			usage
                elif [ \( "$SET_LOG" == true \) ]; then
                        logfile="$var"
                fi
        done
}


# script start
parse_args $@

# running test
#echo "logging to $logfile"
if [ "$logfile" != "" ]; then
	echo --- TEST STARTED --- >>$logfile
	echo Date: $now >>$logfile
	echo "Date (formatted): $nowFormatted" >>$logfile
	$executable >>$logfile
	echo --- TEST FINISHED --- >>$logfile
else
	echo --- TEST STARTED ---
	echo Date: $now
	echo "Date (formatted): $nowFormatted"
	$executable
	echo --- TEST FINISHED ---
fi
