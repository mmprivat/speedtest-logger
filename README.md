# Speedtest - Speedtest Logger

This project is part of my project [speedtest](https://bitbucket.org/mmprivat/workspace/projects/SPDTST) and contains a commandline script for running an internet connection speedtest and logging the result into a file.

The logfile can be transformed into a chart by the other part of the project [graph-transformator](https://bitbucket.org/mmprivat/graph-transformator).

This is the commandline interface of my internet speedtest

* speedtest-cli is the phyton program which does the real work (taken from https://github.com/sivel/speedtest-cli)
* speedtest-logger.sh is my bash program, which logs to a file in a format my "graph-transformator" can parse

So far there is no windows support, sorry ;-) But with the Linux console built into Windows 10 it should work as well. Or xou can use docker containers to run the logger.

## Source Code

Can be found [here](https://bitbucket.org/mmprivat/speedtest-logger)

## Prerequisites
* linux
* phyton

## Run once

execute 

	# speedtest-logger.sh -l PATH_TO_YOUR_LOGFILE

## Run periodically

Edit your cron file

	$ crontab -e

add the line (replace the variables $PATH_TO_LOGGER and $PATH_TO_LOGFILE)

	*/15 * * * * /$PATH_TO_LOGGER/speedtest-logger/speedtest-logger.sh -l /$PATH_TO_LOGFILE/speedtest.log

to log a speedtest every 15 Minutes.

## License

This software project is licensed under the Apache Software Foundation license, version 2.0.